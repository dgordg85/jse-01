package ru.kozyrev.tm;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.TaskManager;
import ru.kozyrev.tm.enumerated.MessageEnum;

import java.util.*;

public class Main {
    static {
        new Project();
        new Task();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(MessageEnum.WELCOME);
        String command;
        while (!(command = sc.nextLine()).equals("exit")) {
            switch (command) {
                case "help":
                    TaskManager.printAllCommands();
                    break;
                case "project-clear":
                    Project.clear();
                    break;
                case "project-update":
                    Project.update();
                    break;
                case "project-create":
                    Project.create();
                    break;
                case "project-list":
                    Project.printList();
                    break;
                case "project-remove":
                    Project.remove();
                    break;
                case "task-clear":
                    Task.clear();
                    break;
                case "task-update":
                    Task.update();
                    break;
                case "task-create":
                    Task.create();
                    break;
                case "task-list":
                    Task.printList();
                    break;
                case "task-remove":
                    Task.remove();
                    break;
                default:
                    System.out.println(MessageEnum.ERROR);
            }
        }
        sc.close();
    }
}
