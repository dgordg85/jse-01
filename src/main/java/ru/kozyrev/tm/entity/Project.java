package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.enumerated.ProjectEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Project extends TaskManager {
    private static List<Project> listProjects = new ArrayList<>();

    static {
        commandList.put("project-clear", "Remove all projects.");
        commandList.put("project-create", "Create new project.");
        commandList.put("project-list", "Show all projects.");
        commandList.put("project-remove", "Remove Selected project.");
        commandList.put("project-update", "Update Selected project.");
    }

    public Project() {
    }

    public Project(String name) {
        super(name);
    }

    public static void create() {
        Scanner sc = new Scanner(System.in);
        System.out.println(ProjectEnum.CREATE);
        String name = sc.nextLine();
        if (name.length() != 0) {
            listProjects.add(new Project(name));
            System.out.println(ProjectEnum.OK);
        } else {
            System.out.println(ProjectEnum.EMPTY);
        }
    }

    public static void printList() {
        System.out.println(ProjectEnum.LIST);
        for (int i = 0; i < listProjects.size(); i++) {
            System.out.printf("%d. %s\n", i + 1, listProjects.get(i));
        }
    }

    public static void remove() {
        Scanner sc = new Scanner(System.in);
        System.out.println(ProjectEnum.REMOVE);
        try {
            listProjects.remove(Integer.parseInt(sc.nextLine()) - 1);
            System.out.println(ProjectEnum.OK);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println(ProjectEnum.IOOB);
        }
    }

    public static void clear() {
        System.out.println(ProjectEnum.CLEAR);
        listProjects.clear();
    }

    public static void update() {
        Scanner sc = new Scanner(System.in);
        System.out.println(ProjectEnum.UPDATE);
        try {
            Project prj = listProjects.get(Integer.parseInt(sc.nextLine()) - 1);
            System.out.println(ProjectEnum.NEW_NAME);
            String name = sc.nextLine();
            if (name.length() != 0) {
                prj.setName(name);
                System.out.println(ProjectEnum.OK);
            } else {
                System.out.println(ProjectEnum.EMPTY);
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println(ProjectEnum.IOOB);
        }
    }
}