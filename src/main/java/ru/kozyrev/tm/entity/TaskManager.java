package ru.kozyrev.tm.entity;

import java.util.HashMap;
import java.util.Map;

public class TaskManager {
    static Map<String, String> commandList;
    String name;

    static {
        commandList = new HashMap<>();
        commandList.put("help", "Show all commands");
        commandList.put("exit", "Quit from manager.");
    }

    public TaskManager() {
    }

    public TaskManager(String name) {
        this.name = name;
    }

    public static void printAllCommands() {
        for (Map.Entry<String, String> command : commandList.entrySet()) {
            System.out.println(command.getKey() + ": " + command.getValue());
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}