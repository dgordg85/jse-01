package ru.kozyrev.tm.entity;

import ru.kozyrev.tm.enumerated.TaskEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task extends TaskManager {
    private static List<Task> listTasks = new ArrayList<>();

    static {
        commandList.put("task-clear", "Remove all tasks.");
        commandList.put("task-create", "Create new tasks.");
        commandList.put("task-list", "Show all tasks.");
        commandList.put("task-remove", "Remove selected task.");
        commandList.put("task-update", "Update Selected task.");
    }

    public Task() {
    }

    public Task(String name) {
        super(name);
    }

    public static void create() {
        Scanner sc = new Scanner(System.in);
        System.out.println(TaskEnum.CREATE);
        String name = sc.nextLine();
        if (name.length() != 0) {
            listTasks.add(new Task(name));
            System.out.println(TaskEnum.OK);
        } else {
            System.out.println(TaskEnum.EMPTY);
        }
    }

    public static void printList() {
        System.out.println(TaskEnum.LIST);
        for (int i = 0; i < listTasks.size(); i++) {
            System.out.printf("%d. %s\n", i + 1, listTasks.get(i));
        }
    }

    public static void remove() {
        Scanner sc = new Scanner(System.in);
        System.out.println(TaskEnum.REMOVE);
        try {
            listTasks.remove(Integer.parseInt(sc.nextLine()) - 1);
            System.out.println(TaskEnum.OK);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println(TaskEnum.IOOB);
        }
    }

    public static void clear() {
        System.out.println(TaskEnum.CLEAR);
        listTasks.clear();
    }

    public static void update() {
        Scanner sc = new Scanner(System.in);
        System.out.println(TaskEnum.UPDATE);
        try {
            Task task = listTasks.get(Integer.parseInt(sc.nextLine()) - 1);
            System.out.println(TaskEnum.NEW_NAME);
            String name = sc.nextLine();
            if (name.length() != 0) {
                task.setName(name);
                System.out.println(TaskEnum.OK);
            } else {
                System.out.println(TaskEnum.EMPTY);
            }
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.out.println(TaskEnum.IOOB);
        }
    }
}