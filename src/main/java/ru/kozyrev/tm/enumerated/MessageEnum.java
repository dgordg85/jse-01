package ru.kozyrev.tm.enumerated;

public enum MessageEnum {
    WELCOME("*** WELCOME TO TASK MANAGER ***"),
    ERROR("Wrong command! Use 'help'!");

    private String message;

    MessageEnum(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
