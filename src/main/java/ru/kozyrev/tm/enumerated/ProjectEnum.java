package ru.kozyrev.tm.enumerated;

public enum ProjectEnum {
    CREATE("[PROJECT CREATE]\nENTER NAME:"),
    LIST("[PROJECTS LIST]"),
    CLEAR("[ALL PROJECTS REMOVED]"),
    REMOVE("[PROJECT DELETE]\nENTER ID:"),
    UPDATE("[UPDATE PROJECT]\nENTER ID:"),
    NEW_NAME("[UPDATE PROJECT]\nENTER NEW NAME:"),
    OK("[OK]"),
    IOOB("[WRONG INDEX]"),
    EMPTY("[EMPTY NAME]");

    private String message;

    ProjectEnum(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
