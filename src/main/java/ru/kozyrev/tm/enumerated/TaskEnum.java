package ru.kozyrev.tm.enumerated;

public enum TaskEnum {
    CREATE("[TASK CREATE]\nENTER NAME:"),
    LIST("[TASKS LIST]"),
    CLEAR("[ALL TASKS REMOVED]"),
    REMOVE("[TASK DELETE]\nENTER ID:"),
    UPDATE("[UPDATE TASK]\nENTER ID:"),
    NEW_NAME("[UPDATE TASK]\nENTER NEW NAME:"),
    OK("[OK]"),
    IOOB("[WRONG INDEX]"),
    EMPTY("[EMPTY NAME]");


    private String message;

    TaskEnum(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
